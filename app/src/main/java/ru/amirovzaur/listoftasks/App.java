package ru.amirovzaur.listoftasks;

import android.app.Application;

/**
 * Created by Zaur on 14.12.2015.
 */
public class App extends Application {
    private static App instance;

    public static App getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setInstance(this);
    }

    private static void setInstance(final App instance) {
        App.instance = instance;
    }
}
