package ru.amirovzaur.listoftasks;

import android.accounts.AccountManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.AccountPicker;

import java.io.IOException;

import ru.amirovzaur.listoftasks.utils.Const;
import ru.amirovzaur.listoftasks.utils.Utils;

/**
 * Created by Zaur on 14.12.2015.
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "LoginActivity";

    static final int REQUEST_CODE_PICK_ACCOUNT = 1000;
    static final int REQUEST_AUTHORIZATION = 1001;

    String mEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retrieve_access_token);
        findViewById(R.id.button_token).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.button_token) {
            pickUserAccount();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PICK_ACCOUNT) {
            // Receiving a result from the AccountPicker
            if (resultCode == RESULT_OK) {
                mEmail = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                ((TextView) findViewById(R.id.gmail_value)).setText(mEmail);
                // With the account name acquired, go get the auth token
                getToken();
            } else if (resultCode == RESULT_CANCELED) {
                // The account picker dialog closed without selecting an account.
                // Notify users that they must pick an account to proceed.
                Toast.makeText(this, "onActivityResult RESULT_CANCELED", Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == REQUEST_AUTHORIZATION && resultCode == RESULT_OK) {
            // Receiving a result that follows a GoogleAuthException, try auth again
            getToken();
        }
    }

    private void pickUserAccount() {
        String[] accountTypes = new String[]{"com.google"};
        Intent intent = AccountPicker.newChooseAccountIntent(null, null,
                accountTypes, false, null, null, null, null);
        startActivityForResult(intent, REQUEST_CODE_PICK_ACCOUNT);
    }

    /**
     * Attempts to retrieve the username.
     * If the account is not yet known, invoke the picker. Once the account is known,
     * start an instance of the AsyncTask to get the auth token and do work with it.
     */
    private void getToken() {
        if (mEmail == null) {
            pickUserAccount();
        } else {
            new RetrieveTokenTask().execute(mEmail);
        }
    }

    public class RetrieveTokenTask extends AsyncTask<String, Void, String[]> {

        private static final String TAG = "RetrieveTokenTask";

        @Override
        protected String[] doInBackground(String... params) {
            String accountName = params[0];
            String scopes = "oauth2:profile email";
            String token = null;
            String accountId = null;
            try {
                token = GoogleAuthUtil.getToken(App.getInstance().getApplicationContext(), accountName, scopes);
                accountId = GoogleAuthUtil.getAccountId(App.getInstance().getApplicationContext(), accountName);
                        Utils.logE(TAG, "token = " + token);
                Utils.logE(TAG, "AccountId = " +accountId);
            } catch (IOException e) {
                Utils.logE(TAG, e.getMessage());
            } catch (UserRecoverableAuthException e) {
                Utils.logE(TAG, e.getMessage());
                //for permission in Google Auth
                startActivityForResult(e.getIntent(), REQUEST_AUTHORIZATION);
            } catch (GoogleAuthException e) {
                Utils.logE(TAG, e.getMessage());
            }
            return new String[]{token, accountId};
        }

        @Override
        protected void onPostExecute(String[] s) {
            super.onPostExecute(s);
            ((TextView) findViewById(R.id.token_value)).setText("Google ID: " + s[1]+"\nToken Value: " + s[0]);
            if (s != null) {
                Intent intent = new Intent();
                intent.putExtra(Const.AUTH_GOOGLE_TOKEN, s[0]);
                intent.putExtra(Const.GOOGLE_ID, s[1]);
                setResult(RESULT_OK,intent);
                finish();
            }
        }
    }
}
