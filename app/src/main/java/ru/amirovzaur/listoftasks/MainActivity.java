package ru.amirovzaur.listoftasks;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.AuthData;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ru.amirovzaur.listoftasks.utils.Const;
import ru.amirovzaur.listoftasks.utils.Utils;

public class MainActivity extends AppCompatActivity implements TaskAdapter.doSomethingTaskCallback {

    private Firebase ref;
    private String googleToken;
    private String googleId;
    private ArrayList<Task> listOfTasks = new ArrayList<Task>();
    private ListView listView;
    private TaskAdapter taskAdapter;
    private final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Firebase.setAndroidContext(this);
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivityForResult(intent, Const.AUTH_GOOGLE_REQUEST);

        (findViewById(R.id.button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddTaskDialog();
            }
        });
        listView = (ListView) findViewById(R.id.listView);
        taskAdapter = new TaskAdapter(this, listOfTasks);
        listView.setAdapter(taskAdapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Const.AUTH_GOOGLE_REQUEST && resultCode == RESULT_OK) {
            googleToken = data.getStringExtra(Const.AUTH_GOOGLE_TOKEN);
            googleId = data.getStringExtra(Const.GOOGLE_ID);
            Utils.logW(TAG, googleToken);
            ref = new Firebase(Const.HTTPS_URL + googleId);
            ref.authWithOAuthToken("google", googleToken, new Firebase.AuthResultHandler() {
                @Override
                public void onAuthenticated(AuthData authData) {
                    // the Google user is now authenticated with your Firebase app
                    Utils.logW(TAG, authData.getUid());
                    ref.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Utils.logD(TAG, "ChildrenCount = " + dataSnapshot.getChildrenCount());
                            if (dataSnapshot.getChildrenCount() != 0) {
                                (findViewById(R.id.textView)).setVisibility(View.GONE);
                                listOfTasks.clear();
                                for (DataSnapshot taskSnapshot : dataSnapshot.getChildren()) {
                                    Task task = taskSnapshot.getValue(Task.class);
                                    Utils.logD(TAG, task.getNameTask() + " " + task.getIsCompleted());
                                    listOfTasks.add(task);
                                    //Utils.logD(TAG, postSnapshot.toString());
                                }
                                taskAdapter.notifyDataSetChanged();
                            } else {
                                (findViewById(R.id.textView)).setVisibility(View.VISIBLE);
                                ((TextView) findViewById(R.id.textView)).setText(R.string.no_tasks);
                            }
                        }

                        @Override
                        public void onCancelled(FirebaseError firebaseError) {
                            Utils.logD(TAG, firebaseError.getMessage());
                            Utils.logD(TAG, firebaseError.getDetails());
                        }
                    });

                    ref.addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            Utils.logW(TAG, "- - - - - - - onChildAdded- - - - - - - -");
                            Utils.logW(TAG, dataSnapshot.toString());
                            if (!listOfTasks.contains(dataSnapshot.getValue(Task.class))) {
                                listOfTasks.add(dataSnapshot.getValue(Task.class));
                                (findViewById(R.id.textView)).setVisibility(View.GONE);
                                taskAdapter.notifyDataSetChanged();
                            }
                            Utils.logW(TAG, "- - - - - - - - - - - - - -");
                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                            Utils.logW(TAG, "- - - - - - onChildChanged - - - - - - -");
                            Utils.logW(TAG, dataSnapshot.toString());
                            //if (!listOfTasks.contains(dataSnapshot.getValue(Task.class))) {
                            for (int i = 0; i < listOfTasks.size(); i++) {
                                Task task = listOfTasks.get(i);
                                if (task.getNameTask().equals(dataSnapshot.getValue(Task.class).getNameTask())) {
                                    Utils.logW(TAG, "onChildChanged");
                                    listOfTasks.get(i).setCompletedTask(dataSnapshot.getValue(Task.class).getIsCompleted());
                                    taskAdapter.notifyDataSetChanged();
                                }
                            }
                            //}
                            Utils.logW(TAG, "- - - - - - - - - - - - - -");
                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {
                            Utils.logW(TAG, "- - - - - - onChildRemoved - - - - - -");
                            Utils.logW(TAG, dataSnapshot.toString());
                            for (int i = 0; i < listOfTasks.size(); i++) {
                                Task task = listOfTasks.get(i);
                                if (task.getNameTask().equals(dataSnapshot.getValue(Task.class).getNameTask())) {
                                    Utils.logW(TAG, "onChildRemoved");
                                    listOfTasks.remove(i);
                                    if (listOfTasks.size() == 0) {
                                        (findViewById(R.id.textView)).setVisibility(View.VISIBLE);
                                        ((TextView) findViewById(R.id.textView)).setText(R.string.no_tasks);
                                    }
                                    taskAdapter.notifyDataSetChanged();
                                }
                            }
                            Utils.logW(TAG, "- - - - - - - - - - - - - -");
                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                            Utils.logW(TAG, "- - - - - - onChildMoved - - - - - -");
                            Utils.logW(TAG, "- - - - - - - - - - - - - -");
                        }

                        @Override
                        public void onCancelled(FirebaseError firebaseError) {
                            Utils.logW(TAG, "- - - - - - onCancelled - - - - - - - -");
                            Utils.logD(TAG, firebaseError.getMessage());
                            Utils.logD(TAG, firebaseError.getDetails());
                            Utils.logW(TAG, "- - - - - - - - - - - - - -");
                        }
                    });
                }

                @Override
                public void onAuthenticationError(FirebaseError firebaseError) {
                    // there was an error
                    Utils.logW(TAG, firebaseError.getMessage() + " " + firebaseError.getDetails());
                    Toast.makeText(MainActivity.this, firebaseError.getMessage() + " "
                            + firebaseError.getDetails(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    public void onDoSomethingTask(int position, boolean completedTask, boolean remove) {
        if (remove) {
            showDeleteTaskDialog(position);
        } else {
            Utils.logE(TAG, listOfTasks.get(position).getNameTask() + " " + completedTask);
            Firebase taskRef = ref.child(listOfTasks.get(position).getNameTask());
            Map<String, Object> taskCompleted = new HashMap<String, Object>();
            taskCompleted.put("isCompleted", completedTask);
            taskRef.updateChildren(taskCompleted);
        }
    }

    public void showAddTaskDialog() {
        AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
        final EditText edittext = new EditText(MainActivity.this);
        alert.setMessage(R.string.dlg_add_message);
        alert.setTitle(R.string.dlg_add_title);
        alert.setView(edittext);
        alert.setPositiveButton(R.string.dlg_add_btn_add, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Editable YouEditTextValue = edittext.getText();
                Firebase taskRef = ref.child(YouEditTextValue.toString());
                taskRef.child("nameTask").setValue(YouEditTextValue.toString());
                taskRef.child("isCompleted").setValue(false);
            }
        });
        alert.setNegativeButton(R.string.dlg_add_btn_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //nothing
            }
        });
        alert.show();
    }

    public void showDeleteTaskDialog(final int position) {
        AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
        alert.setMessage(getString(R.string.dlg_delete_message) + " " + listOfTasks.get(position).getNameTask());
        alert.setTitle(R.string.dlg_delete_title);
        alert.setPositiveButton(R.string.dlg_delete_btn_add, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Firebase taskRef = ref.child(listOfTasks.get(position).getNameTask());
                taskRef.setValue(null);
            }
        });
        alert.setNegativeButton(R.string.dlg_delete_btn_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //nothing
            }
        });
        alert.show();
    }
}
