package ru.amirovzaur.listoftasks;

/**
 * Created by Zaur on 15.12.2015.
 */
public class Task {

    private String nameTask;
    private boolean isCompleted;

    public Task() {
        //empty
    }


    public String getNameTask() {
        return nameTask;
    }

    public boolean getIsCompleted() {
        return isCompleted;
    }

    public void setCompletedTask(final boolean isCompletedTeask) {
        this.isCompleted = isCompletedTeask;
    }

    public void setNameTask(final String nameTask) {
        this.nameTask = nameTask;
    }
}
