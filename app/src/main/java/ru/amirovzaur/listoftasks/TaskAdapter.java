package ru.amirovzaur.listoftasks;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;

import java.util.ArrayList;

import ru.amirovzaur.listoftasks.utils.Utils;
import ru.amirovzaur.listoftasks.views.BetterCheckBox;

/**
 * Created by Zaur on 15.12.2015.
 */
public class TaskAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater lInflater;
    private ArrayList<Task> listOfTasks;
    private final doSomethingTaskCallback callback;

    public TaskAdapter(Context context, ArrayList<Task> listOfTasks) {
        this.callback = (doSomethingTaskCallback) context;
        this.context = context;
        this.listOfTasks = listOfTasks;
        lInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listOfTasks.size();
    }

    @Override
    public Object getItem(int position) {
        return listOfTasks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // используем созданные, но не используемые view
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.item, parent, false);
        }

        Task p = getTask(position);

        ((TextView) view.findViewById(R.id.textView)).setText(p.getNameTask());

        BetterCheckBox cbCompleted = new BetterCheckBox(context, (CheckBox) view.findViewById(R.id.checkBox));
        cbCompleted.setTag(position);
        cbCompleted.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                Utils.logE("TaskAdapter", listOfTasks.get(position).getNameTask() + " " + isChecked);
                callback.onDoSomethingTask(position, isChecked, false);
            }
        });
        cbCompleted.silentlySetChecked(p.getIsCompleted());

        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Utils.logE("TaskAdapter", "onLongClick");
                        callback.onDoSomethingTask(position, false, true);
                return true;
            }
        });
        return view;
    }

    Task getTask(int position) {
        return ((Task) getItem(position));
    }

    public interface doSomethingTaskCallback {

        public void onDoSomethingTask(final int position, final boolean completedTask, final boolean remove);

    }

}
