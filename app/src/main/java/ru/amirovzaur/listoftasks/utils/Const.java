package ru.amirovzaur.listoftasks.utils;

/**
 * Created by Zaur on 14.12.2015.
 */
public class Const {
    public static String HTTPS_URL = "https://listoftests.firebaseio.com/";
    public static int AUTH_GOOGLE_REQUEST = 1953;
    public static String AUTH_GOOGLE_TOKEN = "TOKEN";
    public static String GOOGLE_ID= "GOOGLE_ID";
}
