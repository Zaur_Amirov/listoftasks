package ru.amirovzaur.listoftasks.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import ru.amirovzaur.listoftasks.App;
import ru.amirovzaur.listoftasks.BuildConfig;


/**
 * Created by Zaur on 14.12.2015.
 */
public class Utils {

    private static final String TAG = "LIST_OF_TASKS";

    /**
     * Check network connection
     *
     * @return
     */
    public static boolean isNetworkEnabled() {
        final ConnectivityManager connMan = (ConnectivityManager) App
                .getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo info = connMan.getActiveNetworkInfo();
        return info != null && info.isConnected();
    }

    /**
     * DBGD == Log debug(Log.d)
     *
     * @param tag
     * @param msg
     */
    public static void logD(final String tag, final String msg) {
        if (BuildConfig.DEBUG) {
            Log.d(tag == null || tag.isEmpty() ? TAG : tag,
                    msg == null || msg.isEmpty() ? "Debug" : msg);
        }
    }

    /**
     * DBGE == Log errors(Log.e)
     *
     * @param tag
     * @param msg
     */
    public static void logE(final String tag, final String msg) {
        if (BuildConfig.DEBUG) {
            Log.e(tag == null || tag.isEmpty() ? TAG : tag,
                    msg == null || msg.isEmpty() ? "Error!" : msg);
        }
    }

    /**
     * DBGE == Log errors(Log.e)
     *
     * @param tag
     * @param msg
     */
    public static void logE(final String tag, final String msg, Throwable tr) {
        if (BuildConfig.DEBUG) {
            Log.e(tag == null || tag.isEmpty() ? TAG : tag,
                    msg == null || msg.isEmpty() ? "Error!" : msg, tr);
        }
    }


    /**
     * DBGW == Log warnings(Log.w)
     *
     * @param tag
     * @param msg
     */
    public static void logW(final String tag, final String msg) {
        if (BuildConfig.DEBUG) {
            Log.w(tag == null || tag.isEmpty() ? TAG : tag,
                    msg == null || msg.isEmpty() ? "Info" : msg);
        }
    }

}
